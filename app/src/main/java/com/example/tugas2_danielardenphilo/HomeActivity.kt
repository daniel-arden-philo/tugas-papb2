package com.example.tugas2_danielardenphilo

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.TextView

class HomeActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)

        val usernameTextView = findViewById<TextView>(R.id.usernameTextView)
        val passwordTextView = findViewById<TextView>(R.id.passwordTextView)
        val username = intent.getStringExtra("username")
        val password = intent.getStringExtra("password")

        usernameTextView.text = "Username: $username"
        passwordTextView.text = "Password: $password"
    }
}
